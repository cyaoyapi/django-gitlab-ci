from django.test import TestCase
from django.urls import reverse


class IndexViewTests(TestCase):
    def test_get_index_page(self):
        """Test that we can reach the index page."""

        response = self.client.get(reverse("index"))
        self.assertEqual(response.status_code, 200)

    def test_index_is_displayed_correctly(self):
        """Test that the index page display the correct text."""

        response = self.client.get(reverse("index"))
        self.assertContains(response, "Hello, Welcome to our website.")
