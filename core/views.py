from django.shortcuts import render


def index(request):
    """View of index page."""

    return render(request, "index.html", {})
